const { src, dest, watch } = require('gulp');
const sass = require('gulp-sass')(require('node-sass'));

function compileSCSS() {
  return src('src/scss/main.scss') // Replace with your SCSS file patterns
    .pipe(sass().on('error', sass.logError)) // Handle compilation errors
    .pipe(dest('dist/css')); // Replace with your output directory
}

function watchSCSS() {
  watch('src/scss/main.scss', compileSCSS); // Watch for changes in SCSS files
}

exports.default = compileSCSS; // Set the default task
exports.watch = watchSCSS; // Export the watch task
