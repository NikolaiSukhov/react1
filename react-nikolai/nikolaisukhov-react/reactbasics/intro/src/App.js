


import './App.css';


import React, { useEffect, useState } from 'react';
import Fetch from './components/Fetch/Fetch';
import Header from './components/Header/Header';

function App() {
  const [star, setStar] = useState(0)
const starsetter = () =>{
  setStar(star+1)
}

  const [number, setNumber] = useState(0);

  const setter = () => {
    setNumber(number + 1);
  };

  return (
    <>
      <Header numberAdded={number} numberClicked={star} />
      <Fetch setter={setter} starset={starsetter} />
    </>
  );
}

export default App;
