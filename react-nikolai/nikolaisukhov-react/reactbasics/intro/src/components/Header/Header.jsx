import Button from "../Button/Button"
import Cart from "./Cart"
import Star from './Star'
export default function Header({numberAdded, numberClicked}){
return(
    <>
    <div className="wrapper-header">
<a href="" className="links">Shop</a>
<a href="" className="links">Men</a>
<a href="" className="links">Woman</a>
<Cart Number={numberAdded} OnClick/>
<Star numberstar={numberClicked}/>

</div>
 </>
)
}