function Button ( {type, classNames, onClick, children}){
    return (
        <div className="buttonWrapper" >
        <button type={type} className={classNames} onClick={onClick}>
            {children}
        </button>
        </div>
    )
    
}
export default Button;