import Modal from "./Modal";
import StarProduct from "./StarProduct";
export default function Product({ item, btn, onClick, modalheader, count ,starclicked  }) {

    const saveToLocalStorage = () => {

      const existingProducts = JSON.parse(localStorage.getItem('selectedProducts')) || [];
  
      const isProductSelected = existingProducts.some((product) => product.Articul === item.Articul);

      if (!isProductSelected) {
        existingProducts.push(item);
        localStorage.setItem('selectedProducts', JSON.stringify(existingProducts));
      }
    };
    return (
      <>
      <div key={item.Articul} className='card'>
        <p><img className='image-img' src={item.Url} alt="product" /></p>
        <h2>{item.Name}</h2>
        <p>Price: {item.Price}</p>
        <p>Articul: {item.Articul}</p>
        <p>Color: {item.color}</p>
        {btn}
        <StarProduct click={starclicked}  />

        <Modal headertype={item.Url} naming={item.Name} price={item.Price} counter={count} onClick={saveToLocalStorage}  />
        
      </div>
    

      </>
    );
  }
  