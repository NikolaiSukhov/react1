import React, { useState, useEffect } from 'react';
import Modal from './Modal';
import Product from './Product';


export default function Fetch({ butn, close, isopn, clickedstar, count,setter,starset }) {
  const [data, setData] = useState(null);
  const [selectedProducts, setSelectedProducts] = useState([]);
  useEffect(() => {
    fetch('./array.json')
      .then((response) => response.json())
      .then((data) => {
        setData(data);
      });
  }, []);
  useEffect(() => {
    
    const storedProducts = JSON.parse(localStorage.getItem('selectedProducts')) || [];
    setSelectedProducts(storedProducts);
  }, []);
  if (data === null) {
    return <div>Loading...</div>;
  }

  return (
    <>
      <div className="wrapper">
        {data.map((item) => (
          <Product key={item.Articul} item={item} btn={butn} onClick={clickedstar } count={setter} starclicked={starset}  />
        ))}
      </div>
    </>
  );
}
