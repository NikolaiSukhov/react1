export default function ModalText({h1, text}){
return(
    <div className="texts">
        <div className="h1">{h1}</div>
        <div className="maintext">{text}</div>
        
    </div>
)
}